
#########################################################################################
# GENERAL
#
kvm_uri = "qemu:///system"
kvm_pool-VMs = "vms"
kvm_pool-bases = "bases"


#########################################################################################
# Network
#
networks = [
     {
       name = "net_ext"
       cidr = "1.50.10.0/24"
       mode = "route"
       bridge = "virbr-net-ext"
       domain = "lablocal"
     },
     {
       name = "net_int"
       cidr = "1.50.20.0/24"
       mode = "none"
       bridge = "virbr-net-int"
       domain = "lablocal"
     },
]


#########################################################################################
# Servers
#

cloudinitname = "cloudinitrhel.iso"

#### With OS, 2 Networks (0 and 1) and 2 data disks
servers_type_R1 = [
  {
    name = "rhel-1"
    running = "true"
    baseimage = "rhel-server-7.6-x86_64-kvm.qcow2"
    rootdisk = "1073741824"
    memory = "4096"
    vcpus = "2"
    mgmt_ip = "1.50.10.2"
    rootdisk = 10737418240
    datadisk-0 = 10737418240
  },
]
