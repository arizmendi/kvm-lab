#!/bin/bash


################# NODES PREDEPLOYMENT


ansible-playbook -vv -i inventory/inventory-rhel playbooks/node-predeploy.yaml -e @vars/rhel/node-predeploy_vars.yaml
if [ $? -ne 0 ];
then
    exit -1
fi
