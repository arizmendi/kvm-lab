 #!/bin/bash



#####################################################################################
## EXT SERVICES VM
#####################################################################################

echo ""
echo "Configuring RHEL..."
echo ""

cd ansible
./configure_rhel.sh
if [ $? -ne 0 ];
then
    cd ..
    exit -1
fi
cd ..
