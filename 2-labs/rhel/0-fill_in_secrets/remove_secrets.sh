#!/bin/bash


echo "Remove secrets (to maintain these templates without private info)"


# Remove ansible.log
find .. -name "ansible.log" -type f -delete


#secrets_file="secrets"


#if [ ! -f $secrets_file ]; then
#    echo ""
#    echo "Secrets file not found: $secrets_file"
#    echo ""
#    exit -1
#fi





## 1-install_kvm





## 2-create_vms


#kvm_ip=$(grep kvm_ip $secrets_file | awk -F \= '{print $2}')
#sed -i "s/qemu+ssh:\/\/root@.*/qemu+ssh:\/\/root@${kvm_ip}\/system\"/g" ../2-create_vms/terraform/lab.tfvars
sed -i "s/qemu+ssh:\/\/root@.*/qemu+ssh:\/\/root@SECRET\/system\"/g" ../2-create_vms/terraform/lab.tfvars


#vms_root_password=$(grep vms_root_password $secrets_file | awk -F \= '{print $2}')
#sed -i "s/ root:.*/ root:$vms_root_password/g" ../2-create_vms/terraform/cloud_init.cfg
sed -i "s/ root:.*/ root:SECRET/g" ../2-create_vms/terraform/cloud_init.cfg


## 3-configure_vms

#rhel_reg_activation_key=$(grep rhel_reg_activation_key $secrets_file | awk -F \= '{print $2}')
#sed -i "s/subs_activationkey:.*/subs_activationkey: $rhel_reg_activation_key/g" ../3-configure_vms/ansible/vars/rhel/node-predeploy_vars.yaml
sed -i "s/subs_activationkey:.*/subs_activationkey: SECRET/g" ../3-configure_vms/ansible/vars/rhel/node-predeploy_vars.yaml

#rhel_reg_org=$(grep rhel_reg_org $secrets_file | awk -F \= '{print $2}')
#sed -i "s/subs_orgid:.*/subs_orgid: $rhel_reg_org/g" ../3-configure_vms/ansible/vars/rhel/node-predeploy_vars.yaml
sed -i "s/subs_orgid:.*/subs_orgid: SECRET/g" ../3-configure_vms/ansible/vars/rhel/node-predeploy_vars.yaml

#rhel_reg_pool=$(grep rhel_reg_pool $secrets_file | awk -F \= '{print $2}')
#sed -i "s/subs_pool:.*/subs_pool: $rhel_reg_pool/g" ../3-configure_vms/ansible/vars/rhel/node-predeploy_vars.yaml
sed -i "s/subs_pool:.*/subs_pool: SECRET/g" ../3-configure_vms/ansible/vars/rhel/node-predeploy_vars.yaml
