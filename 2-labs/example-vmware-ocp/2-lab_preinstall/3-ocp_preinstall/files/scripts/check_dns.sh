#!/bin/bash

#sudo yum install -y bind-utils

domain=$1



echo "*******************************"
echo "    NODES"
echo "*******************************"

names="bootstrap master0 master1 master2 etcd-0 etcd-1 etcd-2  worker0 worker1 worker2"

for i in $names
do
    echo "$i.$domain"
    thisip=$(dig $i.$domain +short)
    echo "Forward entry: $thisip"
    echo "Reverse entry: $(dig -x $thisip +short)"
    echo ""
done


echo "*******************************"
echo "    SRV"
echo "*******************************"

echo "_etcd-server-ssl._tcp.$domain"
dig _etcd-server-ssl._tcp.$domain SRV +short
echo ""


echo "*******************************"
echo "    API"
echo "*******************************"


names="api api-int"

for i in $names
do
    echo "$i.$domain"
    dig $i.$domain +short
    echo ""
done


echo "*******************************"
echo "    Wildcard"
echo "*******************************"

echo "*.apps.$domain"
dig *.apps.$domain +short
echo ""
