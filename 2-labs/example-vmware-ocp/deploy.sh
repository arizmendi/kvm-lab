#!/bin/bash

echo ""
echo "Filling in secrets..."
echo ""
cd 0-fill_in_secrets/
chmod +x fill_in_secrets.sh
./fill_in_secrets.sh
 cd ..




echo "******************************************"
echo "****  RUNNING THE  PRE-INSTALLATION   ****"
echo "******************************************"

echo ""
echo "...preparing RHEL nodes"
echo ""

#ansible-playbook -vv -i inventory 2-lab_preinstall/0-prepare_vms/node-predeploy.yaml -e @vars.yaml

#ansible-playbook -vv -i inventory 2-lab_preinstall/0-prepare_vms/node-predeploy-nosubs.yaml -e @vars.yaml


echo ""
echo ""
echo "Next steps:"
echo ""
echo "  - Check environment:"
echo "         + Check DNS"
echo "         + Check NFS paths"
echo "         + Check LB access"
echo "         + Internet access"
echo "         + Check no network overlap with OCP default networks:"
echo "                  10.128.0.0/14"
echo "                  172.30.0.0/16"
echo ""
echo "  - Create VMware Folder"
echo ""
echo "  - Import OVA"
echo ""
echo "  - Create VM paramaters:"
echo "         + guestinfo.ignition.config.data : insertdata "
echo "         + guestinfo.ignition.config.data.encoding : base64  "
echo "         + disk.EnableUUIDa : TRUE "
echo ""
echo "  - Clone to templates from OVA"
echo "         + master"
echo "         + worker-bootstrap"
echo ""
echo ""
read -p "Press any key when ready..."





# Prepare installer ###########
echo ""
echo "...preparing installer node"
echo ""


#ansible-playbook -vv -i inventory 2-lab_preinstall/3-ocp_preinstall/ocp-installer_host.yaml -e @2-lab_preinstall/3-ocp_preinstall/ocp-install-files/install-config.yaml -e @vars.yaml
#ansible-playbook -vv -i inventory 2-lab_preinstall/3-ocp_preinstall/ocp-installer_config_files.yaml -e @2-lab_preinstall/3-ocp_preinstall/ocp-install-files/install-config.yaml -e @vars.yaml


echo ""
echo ""
echo "Next steps:"
echo ""
echo "  - Create VMs from templates including IGNITION data in base64"
echo ""
echo "  - Check that VMs MAC Addresses are in the VMware range:"
echo "         00:05:69:00:00:00 to 00:05:69:FF:FF:FF"
echo "         00:0c:29:00:00:00 to 00:0c:29:FF:FF:FF"
echo "         00:1c:14:00:00:00 to 00:1c:14:FF:FF:FF"
echo "         00:50:56:00:00:00 to 00:50:56:FF:FF:FF"
echo ""
echo "  - Prepare Helper node:"
echo "         + VM MAC address info for DHCP"
echo ""
echo ""
read -p "Press any key when ready..."





### HELPER #######################################################
echo ""
echo "...preparing helper node"
echo ""

#ansible-playbook -vv -i inventory 2-lab_preinstall/2-ext_services_preinstall/dhcp/ocp-helper-dhcp.yaml -e @2-lab_preinstall/3-ocp_preinstall/ocp-install-files/install-config.yaml  -e @vars.yaml

#ansible-playbook -vv -i inventory 2-lab_preinstall/2-ext_services_preinstall/http/ocp-helper-http.yaml -e @2-lab_preinstall/3-ocp_preinstall/ocp-install-files/install-config.yaml  -e @vars.yaml

#ansible-playbook -vv -i inventory 2-lab_preinstall/2-ext_services_preinstall/haproxy/ocp-helper-haproxy.yaml -e @2-lab_preinstall/3-ocp_preinstall/ocp-install-files/install-config.yaml  -e @vars.yaml


#ansible-playbook -vv -i inventory 2-lab_preinstall/3-ocp_preinstall/ocp-upload-image-files.yaml -e @2-lab_preinstall/3-ocp_preinstall/ocp-install-files/install-config.yaml -e @vars.yaml




echo "************************************"
echo "****  RUNNING THE INSTALLATION  ****"
echo "************************************"
echo ""
echo ""
read -p "Start VMs and, when done, press a key to continue"





echo "*****************************************"
echo "****  RUNNING THE POST-INSTALLATION  ****"
echo "*****************************************"

ansible-playbook -vv -i inventory 4-lab_postinstall/1-ocp_postinstall/ocp-installer-postinstall.yaml -e @2-lab_preinstall/3-ocp_preinstall/ocp-install-files/install-config.yaml  -e @vars.yaml

read -p "Remove bootstrap node from Load Balancer. When done press a key to continue"


ansible-playbook -vv -i inventory 4-lab_postinstall/3-ocp-extra/ocp-extra-steps.yaml -e @2-lab_preinstall/3-ocp_preinstall/ocp-install-files/install-config.yaml  -e @vars.yaml
