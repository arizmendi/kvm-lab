#!/bin/bash


echo "Remove secrets (to maintain these templates without private info)"


# Remove ansible.log
find .. -name "ansible.log" -type f -delete




sed -i "s/subs_activationkey:.*/subs_activationkey: SECRET/g" ../vars.yaml

sed -i "s/subs_orgid:.*/subs_orgid: SECRET/g" ../vars.yaml

sed -i "s/subs_pool:.*/subs_pool: SECRET/g" ../vars.yaml


sed -i "s/subs_user:.*/subs_user: SECRET/g" ../vars.yaml
sed -i "s/subs_pass:.*/subs_pass: SECRET/g" ../vars.yaml


sed -i "s#pullSecret:.*#pullSecret: \'SECRET\'#g" ../2-lab_preinstall/3-ocp_preinstall/ocp-install-files/install-config.yaml
