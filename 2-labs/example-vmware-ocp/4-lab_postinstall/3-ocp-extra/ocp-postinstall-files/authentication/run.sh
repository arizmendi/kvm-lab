 #/bin/bash

# Create htpasswd file with users
htpasswd -c -B -b users.htpasswd clusteradmin redhatrules
htpasswd -b users.htpasswd devuser redhat
htpasswd -b users.htpasswd viewuser redhat

htpasswd -b users.htpasswd dev1 ocp
htpasswd -b users.htpasswd dev2 ocp
htpasswd -b users.htpasswd dev3 ocp
htpasswd -b users.htpasswd dev4 ocp
htpasswd -b users.htpasswd dev5 ocp
htpasswd -b users.htpasswd dev6 ocp
htpasswd -b users.htpasswd dev7 ocp
htpasswd -b users.htpasswd dev8 ocp
htpasswd -b users.htpasswd dev9 ocp
htpasswd -b users.htpasswd dev10 ocp



# Assign htpasswd file to auth provisioner and enable provisioner
/usr/local/bin/oc create secret generic htpass-secret --from-file=htpasswd=users.htpasswd -n openshift-config
/usr/local/bin/oc apply -f descriptors.yaml


# Create cluster admin
/usr/local/bin/oc adm policy add-cluster-role-to-user cluster-admin clusteradmin


# Create groups
/usr/local/bin/oc adm groups new developers devuser dev1 dev2 dev3 dev4 dev5 dev6 dev7 dev8 dev9 dev10
/usr/local/bin/oc adm groups new reviewers viewuser


# Assign roles to groups
/usr/local/bin/oc adm policy add-cluster-role-to-group view reviewers
/usr/local/bin/oc adm policy add-role-to-group admin developers
